
/**
 * Redes Integradas de Telecomunicações II MIEEC 2013/2014
 *
 * rit2ECGI.java
 *
 * Servlet Example - this class implements the javaEPOST interface. It manages a
 * list of groups, using Properties lists. The user receives the current list,
 * and may add or remove groups. INCOMPLETE VERSION
 */
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class rit2EPOST extends JavaEPOST {

    private final String bdname = "grupos.txt";

    private groupDB db;

    /**
     * Creates a new instance of rit2POST
     */
    public rit2EPOST() {
        db = new groupDB(bdname);
    }

    /*
     * Functions that handle cookie related issues
     */
    // To be completed
    /**
     * Select a subset of 'k' number of a set of numbers raging from 1 to 'max'
     */
    private int[] draw_numbers(int max, int k) {
        int[] vec = new int[k];
        int j;

        Random rnd = new Random(System.currentTimeMillis());
        for (int i = 0; i < k; i++) {
            do {
                vec[i] = rnd.nextInt(max) + 1;
                for (j = 0; j < i; j++) {
                    if (vec[j] == vec[i]) {
                        break;
                    }
                }
            } while ((i != 0) && (j < i));
        }
        return vec;
    }

    /**
     * Selects the minimum number in the array
     */
    private int minimum(int[] vec, int max) {
        int min = max + 1, n = -1;
        for (int i = 0; i < vec.length; i++) {
            if (vec[i] < min) {
                n = i;
                min = vec[i];
            }
        }
        if (n == -1) {
            System.err.println("Internal error in rit2EPOST.minimum\n");
            return max + 1;
        }
        vec[n] = max + 1;  // Mark position as used
        return min;
    }

    /**
     * Prepares the rit2ECGI demo web page
     */
    private String make_testPage(String ip, int port, String tipo, String grupo, int n, String n1, String na1, String n2, String na2, String n3, String na3, boolean count, Properties cookies, String lastUpdate) {
        // Draw "lucky" numbers
        int[] set1 = draw_numbers(50, 5);
        int[] set2 = draw_numbers(9, 2);

        // Prepare string html with web page
        String html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n<html>\r\n<head>\r\n";
        html += "<meta content=\"text/html; charset=ISO-8859-1\" http-equiv=\"content-type\">\r\n";
        //html += "<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script>\r\n";
        html += "<title>rit2ECGI.htm</title>\r\n</head>\r\n<body>\r\n";
        html += "<p align=\"center\"><img src=\"gifs/fctunl_big.gif\" border=\"0\" height=\"94\" width=\"600\"></p>\r\n";
        html += "<h1 align=\"center\">P&aacute;gina de teste do servidor JavaECGI</h1>\r\n";
        html += "<h1 align=\"center\"><font color=\"#800000\">RIT II</font> <font color=\"#c0c0c0\">2011/2012</font></h1>\r\n";
        html += "<h3 align=\"center\">1&ordm; Trabalho de laborat&oacute;rio</h3>\r\n";
        html += "<p align=\"left\">Ligou a partir de <font color=\"#ff0000\">" + ip + "</font>:";
        html += "<font color=\"#ff0000\">" + port + "</font> num browser do tipo <font color=\"#ff0000\">" + tipo + "</font>.</p>\r\n";
        if (n >= 0) {
            html += "<p align=\"left\">Os elementos do grupo <font color=\"#0000ff\">" + (grupo.length() > 0 ? (grupo) : "?") + "</font> j&aacute; actualizaram ";
            html += "<font color=\"#0000ff\">" + n + "</font> vezes o grupo no servidor.</p>\r\n";
        }
        if (n >= 0) {
            html += "<p align=\"left\">O &uacute;ltimo acesso ao servidor por este utilizador foi em: "
                    + " <font color=\"#0000ff\">" + lastUpdate + "</font>.</p>\r\n";
        }
        html += "<form method=\"post\" action=\"rit2EPOST.prlt\">\r\n<h3>\r\nDados do grupo</h3>";
        html += "<p>Grupo <input class=textbox name=\"Grupo\" size=\"2\" type=\"text\""
                + (grupo.length() > 0 ? " value=\"" + grupo + "\"" : "") + "></p>\r\n";
        html += "<p>N&uacute;mero <input class=input name=\"Num1\" size=\"5\" type=\"text\""
                + (n1.length() > 0 ? " value=" + n1 : "")
                + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nome <input class=input name=\"Nome1\" size=\"80\" type=\"text\""
                + (na1.length() > 0 ? " value=" + na1 : "")
                + "></p>\r\n";
        html += "<p>N&uacute;mero <input class=input name=\"Num2\" size=\"5\" type=\"text\""
                + (n2.length() > 0 ? " value=" + n2 : "")
                + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nome <input class=input name=\"Nome2\" size=\"80\" type=\"text\""
                + (na2.length() > 0 ? " value=" + na2 : "")
                + "></p>\r\n";
        html += "<p>N&uacute;mero <input class=input name=\"Num3\" size=\"5\" type=\"text\""
                + (n3.length() > 0 ? " value=" + n3 : "")
                + ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nome <input class=input name=\"Nome3\" size=\"80\" type=\"text\""
                + (na3.length() > 0 ? " value=" + na3 : "")
                + "></p>\r\n";
        html += "<p><input name=\"Contador\"" + (count ? " checked=\"checked\"" : "") + " value=\"ON\" type=\"checkbox\">Contador</p>\r\n";
        html += "<p><input value=\"Submeter\" name=\"BotaoSubmeter\" type=\"submit\">";
        html += "<input value=\"Apagar\" name=\"BotaoApagar\" type=\"submit\">";
        html += "<input value=\"Limpar\" type=\"reset\" value=\"Reset\" class=button name=\"BotaoLimpar \">";
        html += "</p>\r\n</form>\r\n";
        html += "<h3>Grupos registados</h3>";
        html += db.table_group_html(cookies);
        html += "<h3>Um exemplo de cont&eacute;udo din&acirc;mico :-)</h3>";
        html += "<p align=\"left\">Se quiser deitar dinheiro fora, aqui v&atilde;o algumas sugest&otilde;es para ";
        html += "o pr&oacute;ximo <a href=\"https://www.jogossantacasa.pt/web/JogarEuromilhoes/?\">Euromilh&otilde;es</a>: ";
        for (int i = 0; i < 5; i++) {
            html += (i == 0 ? "" : " ") + "<font color=\"#00ff00\">" + minimum(set1, 50) + "</font>";
        }
        html += " + <font color=\"#800000\">" + minimum(set2, 9) + "</font> <font color=\"#800000\">" + minimum(set2, 9) + "</font></p>\r\n";
        html += "<p align=\"left\">&nbsp;</p>\r\n";
        html += "<p align=\"left\"><font face=\"Times New Roman\">&copy; 2013/2014</font></p>\r\n</body>\r\n</html>\r\n";
        //html += "<script src=\"jscript.js\"></script>\r\n";
        return html; // HTML page code
    }

    /**
     * Runs GET/HEAD method
     */
    @Override
    public boolean doGet(Socket s, Properties param, Properties cookies, HTTPAnswer ans) {
        System.out.println("run2EPOST GET");

        DateFormat httpformat
                = new SimpleDateFormat("EE,_d_MMM_yyyy_HH:mm:ss_zz", Locale.UK);
        httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String group = "", nam1 = "", n1 = "", nam2 = "", n2 = "", nam3 = "", n3 = "", lastUpdate = "";
        int cnt = -1;

        //Don't forget to convert Names from ECGI format to HTML format before preparing the web page
        String aux = JavaEPOST.String2htmlString(nam1);
        if (aux != null) {
            nam1 = aux;
        }
        aux = JavaEPOST.String2htmlString(nam2);
        if (aux != null) {
            nam2 = aux;
        }
        aux = JavaEPOST.String2htmlString(nam3);
        if (aux != null) {
            nam3 = aux;
        }
        aux = JavaEPOST.String2htmlString(lastUpdate);
        if (aux != null) {
            lastUpdate = aux;
        }

        // Prepare html page
        String html = make_testPage(s.getInetAddress().getHostAddress(), s.getPort(), param.getProperty("user-agent", "Indefinido"),
                group, cnt, n1, nam1, n2, nam2, n3, nam3, false, cookies, lastUpdate);

        // Prepare answer
        ans.set_code(HTTPReplyCode.OK);
        ans.set_text(html);

        return true;
    }

    /**
     * Runs POST method
     */
    @Override
    public boolean doPost(Socket s, Properties param, Properties cookies, Properties fields, HTTPAnswer ans) {
        // Put POST implementation here
        System.out.println("run2EPOST POST");
        String group = fields.getProperty("Grupo", "");
        String nam1 = fields.getProperty("Nome1", "");
        String n1 = fields.getProperty("Num1", "");
        String nam2 = fields.getProperty("Nome2", "");
        String n2 = fields.getProperty("Num2", "");
        String nam3 = fields.getProperty("Nome3", "");
        String n3 = fields.getProperty("Num3", "");
        boolean SubmitButton = (fields.getProperty("BotaoSubmeter") != null);
        boolean DeleteButton = (fields.getProperty("BotaoApagar") != null);
        boolean contador = (fields.getProperty("Contador") != null);
        String lastUpdate = "";

        System.err.println("Button: " + (SubmitButton ? "Submit" : "")
                + (DeleteButton ? "Delete" : "") + "\n");

        int cnt = -1;

        if (SubmitButton && !group.isEmpty()) {
            db.store_group(group, contador, n1, nam1, n2, nam2, n3, nam3);

            ans.buildCookie(group, group);
            cookies.setProperty(group, group);
        }
        
        if(cookies.containsKey(NameDictionary.GETPOST)){
            System.out.println("aquiiiiiiiiiiiiiiiiiii");
            String aux = cookies.getProperty("Group");
            ans.buildCookie(aux, aux);
            cookies.setProperty(aux, aux);
        }

        if (cookies.containsKey(group)) {
            lastUpdate = db.get_group_info(group, "lastUpdate");
            cnt = Integer.parseInt(db.get_group_info(group, ""));
        }

        if (DeleteButton && !group.isEmpty() && cookies.containsKey(group)) {
            db.remove_group(group);
            cnt = -1;
            lastUpdate="";
        }

        String aux = JavaEPOST.String2htmlString(nam1);
        if (aux != null) {
            nam1 = aux;
        }
        aux = JavaEPOST.String2htmlString(nam2);
        if (aux != null) {
            nam2 = aux;
        }
        aux = JavaEPOST.String2htmlString(nam3);
        if (aux != null) {
            nam3 = aux;
        }
        aux = JavaEPOST.String2htmlString(lastUpdate);
        if (aux != null) {
            lastUpdate = aux;
        }

        // Prepare html page
        String html = make_testPage(s.getInetAddress().getHostAddress(), s.getPort(), param.getProperty("user-agent", "Indefinido"),
                group, cnt, n1, nam1, n2, nam2, n3, nam3, (fields.getProperty("Contador") != null), cookies, lastUpdate);

        // Prepare answer
        ans.set_code(HTTPReplyCode.OK);
        ans.set_text(html);

        return true;
    }
}
