
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

/**
 *
 * @author Joaquim
 */
public class HTTPRequest {

    public Properties Requests;
    public Properties cookies;
    public Properties Fields;
    private String code;
    private String file;
    private String ver;

    public HTTPRequest() {
        Requests = new Properties();
        cookies = new Properties();
    }

    public void setCookie(String key, String value) {
        cookies.setProperty(key, value);
    }

    public String getCookie(String key) {
        if (!cookies.containsKey(key)) {
            return null;
        }
        return cookies.getProperty(key);

    }

    public boolean splitCookies(String cookies, boolean isGetPost) {
        String[] split;

        if (isGetPost) {
            split = cookies.split("&");
        } else {
            split = cookies.split(";");
        }

        if (split.length < 0) {
            return false;
        }

        String[] secondsplit;

        for (int i = 0; i < split.length; i++) {
            secondsplit = split[i].split("=");
            if (secondsplit.length == 2) {
                this.cookies.setProperty(secondsplit[0].trim(), secondsplit[1]);
            }
        }

        return true;
    }

    public void buildFields(String fields) {
        Fields = new Properties();
        if (fields.isEmpty()) {
            return;
        }
        String[] equalsplit;
        String[] ecomercial = fields.split("&");

        for (int i = 0; i < ecomercial.length; i++) {
            equalsplit = ecomercial[i].split("=");
            System.out.println("Fields: " + ecomercial[i]);
            if (equalsplit.length != 2) {
                Fields.setProperty(equalsplit[0], "");
            } else {
                Fields.setProperty(equalsplit[0], equalsplit[1]);
            }
        }

    }

    public void ViewCookies() {
        Iterator<Object> it = cookies.keySet().iterator();
        int aux = 1;
        System.out.println("<--------------------------------Teste------------------------------>");
        while (it.hasNext()) {
            String something = it.next().toString();
            System.out.println(aux + " " + something + " " + cookies.getProperty(something));
            aux++;
        }
        System.out.println("<-------------------------------- FIM Teste------------------------------>");
    }

    public void ViewFields() {
        Iterator<Object> it = Fields.keySet().iterator();
        int aux = 1;
        System.out.println("<--------------------------------Teste------------------------------>");
        while (it.hasNext()) {
            String something = it.next().toString();
            System.out.println(aux + " " + something + " " + Requests.getProperty(something));
            aux++;
        }
        System.out.println("<-------------------------------- FIM Teste------------------------------>");
    }

    public boolean isKeepAlive(int timeout) {
        if (!Requests.containsKey(NameDictionary.Connection.toLowerCase())) {
            return false;
        }
        return (GetValue(NameDictionary.Connection).equalsIgnoreCase(NameDictionary.KeepAlive) && timeout > 0);
    }

    //Get and Set off the Code
    public void setCode(String Code) {
        this.code = Code;
    }

    public String getCode() {
        return this.code;
    }

    //Get and Set off the File
    public void setFile(String File) {
        this.file = File;
    }

    public String getFile() {
        return this.file;
    }

    //Get and Set off the Version
    public void setVer(String Ver) {
        this.ver = Ver;
    }

    public String getVer() {
        return this.ver;
    }

    public boolean PutValue(String key, String value) {
        if (Requests.contains(key.toLowerCase())) {
            return false;
        }
        Requests.put(key.toLowerCase(), value);
        return true;
    }

    public String GetValue(String key) {
        return Requests.getProperty(key.toLowerCase());
    }

    public Boolean IsModified(File f) {

        Boolean IsModified = false;
        Boolean IsETAGValid = false;

        if (Requests.containsKey(NameDictionary.IfModifiedSince.toLowerCase())) {
            DateFormat httpformat = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
            httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));

            IsModified = httpformat.format(f.lastModified()).
                    equals(Requests.get(NameDictionary.IfModifiedSince.toLowerCase()));
        }

        if (Requests.containsKey(NameDictionary.IfNoneMatch.toLowerCase())) {
            IsETAGValid = Requests.getProperty(NameDictionary.IfNoneMatch.toLowerCase()).
                    equalsIgnoreCase(Integer.toString(f.hashCode()));
        }

        return (IsModified && IsETAGValid);
    }

    // JUST FOR DEBIUGING
    public void View() {
        Iterator<Object> it = Requests.keySet().iterator();
        int aux = 1;
        System.out.println("<--------------------------------Teste------------------------------>");
        System.out.println(code + " " + file + " " + ver);
        while (it.hasNext()) {
            String something = it.next().toString();
            System.out.println(aux + " " + something + " " + Requests.getProperty(something));
            aux++;
        }
        System.out.println("<-------------------------------- FIM Teste------------------------------>");
    }

    void getFile(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
