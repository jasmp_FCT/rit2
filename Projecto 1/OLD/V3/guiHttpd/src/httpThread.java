
/**
 * Redes Integradas de Telecomunicações II MIEEC 2013/2014
 *
 * httpThread.java
 *
 * Class that handles client's requests. It must handle HTTP GET, HEAD and POST
 * client requests INCOMPLETE VERSION
 *
 */
import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class httpThread extends Thread {

    guiHttpd root;
    ServerSocket ss;
    Socket client;
    DateFormat httpformat;

    /**
     * Creates a new instance of httpThread
     */
    public httpThread(guiHttpd root, ServerSocket ss, Socket client) {
        this.root = root;
        this.ss = ss;
        this.client = client;
        httpformat = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
        httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));
        setPriority(NORM_PRIORITY - 1);
    }

    /**
     * The type for unguessable files
     */
    String guessMime(String fn) {
        String lcname = fn.toLowerCase();
        int extenStartsAt = lcname.lastIndexOf('.');
        if (extenStartsAt < 0) {
            if (fn.equalsIgnoreCase("makefile")) {
                return "text/plain";
            }
            return "unknown/unknown";
        }
        String exten = lcname.substring(extenStartsAt);
        // System.out.println("Ext: "+exten);
        if (exten.equalsIgnoreCase(".htm")) {
            return "text/html";
        } else if (exten.equalsIgnoreCase(".html")) {
            return "text/html";
        } else if (exten.equalsIgnoreCase(".gif")) {
            return "image/gif";
        } else if (exten.equalsIgnoreCase(".jpg")) {
            return "image/jpeg";
        } else {
            return "application/octet-stream";
        }
    }

    public void Log(boolean in_window, String s) {
        if (in_window) {
            root.Log("" + client.getInetAddress().getHostAddress() + ";"
                    + client.getPort() + "  " + s);
        } else {
            System.out.print("" + client.getInetAddress().getHostAddress()
                    + ";" + client.getPort() + "  " + s);
        }
    }

    /**
     * Loads a JavaServlet class into the VM name : "Classname.prlt"
     */
    private JavaEPOST start_POSTserver(String name) {
        // Removes the trailing ".prlt" from the name
        name = name.substring(name.lastIndexOf(java.io.File.separatorChar) + 1, name.length() - 5);
        JavaEPOST post = null;
        try {
            Class postClass = Class.forName(name);
            Object postObject = postClass.newInstance();
            post = (JavaEPOST) postObject;
        } catch (ClassNotFoundException e) {
            System.err.println("POST class not found:" + e);
        } catch (InstantiationException e) {
            System.err.println("POST class instantiation:" + e);
        } catch (IllegalAccessException e) {
            System.err.println("POST class access:" + e);
        }
        return post;
    }

    @Override
    public void run() {

        HTTPAnswer ans;   // HTTP answer object
        PrintStream pout;
        HTTPRequest req;
        int timeout;
        boolean isKeepAlive = false;
        boolean isGetPost = false;
        String request;
        StringTokenizer st;

        try {
            InputStream in = client.getInputStream();
            BufferedReader bin = new BufferedReader(
                    new InputStreamReader(in, "8859_1"));
            OutputStream out = client.getOutputStream();
            pout = new PrintStream(out, false, "8859_1");

            do {

                try {
                    timeout = root.getKeepAlive();

                    request = bin.readLine();

                    if (request == null) {
                        isKeepAlive = false;
                    } else {
                        st = new StringTokenizer(request);
                        Log(true, "Request: " + request + "\n");

                        if (st.countTokens() != 3) {
                            return;
                        }

                        req = new HTTPRequest();
                        req.setCode(st.nextToken());
                        req.setFile(st.nextToken());
                        req.setVer(st.nextToken());

                        if (req.getFile().indexOf('?') >= 0 && req.getCode().equalsIgnoreCase(NameDictionary.GET)) {
                            req.setCode(NameDictionary.GETPOST);
                            String[] aux = req.getFile().split("\\?");
                            req.setFile("/" + aux[0].replace('/', ' ').trim());
                            req.splitCookies(aux[1], isGetPost);
                            isGetPost = true;
                        } else {
                            isGetPost = false;
                        }

                        while (true) {
                            request = bin.readLine();
                            if (request.isEmpty()) {
                                break;
                            }
                            Log(true, "Request: " + request + "\n");

                            String[] splitter = request.split(":", 2);
                            if (splitter[0].equalsIgnoreCase(NameDictionary.cookie)) {
                                req.splitCookies(splitter[1], isGetPost);
                            } else {
                                req.PutValue(splitter[0].trim(), splitter[1].trim());
                            }
                        }

                        ans = new HTTPAnswer(root,
                                client.getInetAddress().getHostAddress() + ":" + client.getPort(),
                                guiHttpd.server_name + " - " + InetAddress.getLocalHost().getHostName() + "-" + root.server.getLocalPort());

                        ans.set_version(req.getVer());
                        ans.set_Date();
                        isKeepAlive = activeKeepAlive(ans, timeout, req);

                        switch (req.getCode().toUpperCase()) {

                            case NameDictionary.HEAD:
                            case NameDictionary.GET:
                                if (IsPRLT(req.getFile())) {
                                    PRLTGet(pout, ans, req);
                                } else {
                                    HeadANDGet(pout, ans, req);
                                }
                                break;
                            case NameDictionary.GETPOST:
                            case NameDictionary.POST:
                                POST(pout, ans, req, bin, isGetPost);
                                break;

                            default:
                                buildNotImplemented(pout, ans, req);
                                break;
                        }

                    }
                } catch (SocketTimeoutException ex) {
                    isKeepAlive = false;
                    Log(true, "Timeout: " + ex.getMessage() + "\n");
                } catch (SocketException e) {
                    //e.printStackTrace(System.err);
                    isKeepAlive = false;
                }

            } while (isKeepAlive);

            in.close();
            pout.close();
            out.close();

        } catch (IOException e) {
            if (root.active()) {
                System.out.println("I/O error " + e);
                e.printStackTrace(System.err);
            }
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                // Ignore
                System.out.println("Error closing client" + e);
            }
            root.thread_ended();
            Log(true, "Closed TCP connection\n");
        }
    }

    private void HeadANDGet(PrintStream pout, HTTPAnswer ans, HTTPRequest req) throws IOException {

        String filename = root.getRaizHtml() + req.getFile() + (req.getFile().equals("/") ? "index.htm" : "");
        System.out.println("Filename= " + filename);
        File f = new File(filename);

        if (f.exists() && f.isFile()) {

            ans.set_file(new File(filename), guessMime(filename));

            if (req.IsModified(f)) {
                ans.set_code(HTTPReplyCode.NOTMODIFIED);
            } else {
                ans.set_code(HTTPReplyCode.OK);
            }

        } else {
            System.out.println("File not found");
            ans.set_error(HTTPReplyCode.NOTFOUND, req.getVer());
        }

        //Send Reply
        if (req.getCode().equalsIgnoreCase(NameDictionary.HEAD)) {
            ans.send_Answer(pout, false, true);

        } else {
            ans.send_Answer(pout, true, true);
        }
    }

    private void PRLTGet(PrintStream pout, HTTPAnswer ans, HTTPRequest req) throws IOException {
        JavaEPOST prlt = start_POSTserver(PRLTfileName(req.getFile()));
        prlt.doGet(client, req.Requests, req.cookies, ans);
        ans.send_Answer(pout, true, true);
    }

    private void POST(PrintStream pout, HTTPAnswer ans, HTTPRequest req, BufferedReader bin, boolean isGetPOST) throws IOException {
        String fields= "";
        if (!isGetPOST) {
            char aux[] = new char[Integer.parseInt(req.GetValue(NameDictionary.ContentLength))];
            fields = JavaEPOST.postString2string(String.valueOf(aux));

            if (bin.read(aux,
                    0, aux.length) < 0) {
                return;
            }
        } else {
            req.setCookie(NameDictionary.GETPOST, " ");
        }

        req.buildFields(fields);

        JavaEPOST prlt = start_POSTserver(PRLTfileName(req.getFile()));

        //req.ViewFields();
        req.ViewCookies();

        prlt.doPost(client, req.Requests, req.cookies, req.Fields, ans);

        ans.send_Answer(pout,
                true, true);
    }

    private void buildNotImplemented(PrintStream pout, HTTPAnswer ans, HTTPRequest req) throws IOException {
        Log(true, "NOT IMPLEMENTED!\n");
        ans.set_error(HTTPReplyCode.NOTIMPLEMENTED, req.getVer());
        ans.send_Answer(pout, true, true);
    }

    private Boolean IsPRLT(String file) {
        System.out.println(file);
        String[] splitter = file.split("\\.");
        if (splitter.length != 2) {
            return false;
        }
        return splitter[1].trim().equalsIgnoreCase(NameDictionary.PRLT);
    }

    private String PRLTfileName(String file) {
        return file.replace("/", "");
    }

    private boolean activeKeepAlive(HTTPAnswer ans, int timeout, HTTPRequest req) {
        try {
            if (req.isKeepAlive(timeout)) {
                ans.set_KeepAlive(timeout);
                if (timeout > 0) {
                    Log(true, "TIMEOUT Active: " + timeout + "\n");
                    client.setSoTimeout(timeout);
                }

                return true;
            }
        } catch (SocketException ex) {
            System.err.println("ERROR in activeKeepAlive: " + ex);
        }

        return false;

    }

}
