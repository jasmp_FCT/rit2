/**
 *
 * @author Joaquim
 */
public class NameDictionary {

    public static final String Connection = "Connection";
    public static final String KeepAlive = "keep-Alive";
    public static final String IfModifiedSince = "if-Modified-Since";
    public static final String Date = "Date";
    public static final String Server = "Server";
    public static final String LastModified = "Last-Modified";
    public static final String ContentType = "Content-Type";
    public static final String ContentLength = "Content-Length";
    public static final String ContentEncoding = "Content-Encoding";
    public static final String ETAG = "ETAG";
    public static final String Encoding = "ISO-8859-1";
    public static final String HTTP1_1 = "HTTP/1.1";
    public static final String IfNoneMatch = "If-None-Match";
    public static final String CacheControl = "Cache-Control";

    public static final String GET = "GET";
    public static final String HEAD = "HEAD";
    public static final String POST = "POST";
    public static final String GETPOST = "GET?POST";
    public static final String PRLT = "prlt";
}
