
/**
 * Redes Integradas de Telecomunicações II MIEEC 2013/2014
 *
 * HTTPAnswer.java
 *
 * Class that stores all information about a HTTP reply INCOMPLETE VERSION
 *
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class HTTPAnswer {

    /**
     * Reply code information
     */
    public HTTPReplyCode code;
    /**
     * Reply headers data
     */
    public Properties param;                // Header fields
    public ArrayList<String> set_cookies;   // Set cookies header fields
    /**
     * Reply contents They are stored either in a text buffer or in a file
     */
    public String text; // buffer with reply contents 
    public File file;   // file used if text == null

    Log log;
    String id_str;  // Thread id - for logging purposes

    /**
     * Creates a new instance of HTTPAnswer
     */
    public HTTPAnswer(Log log, String id_str, String server_name) {
        this.code = new HTTPReplyCode();
        this.id_str = id_str;
        this.log = log;
        param = new Properties();
        set_cookies = new ArrayList<String>();
        text = null;
        file = null;
        /**
         * define Server header field name
         */
        param.setProperty("Server", server_name);
    }

    /**
     * Creates a proxy copy of a HTTPQuery
     */
    public HTTPAnswer(HTTPAnswer a) {
        code = new HTTPReplyCode(a.code);
        id_str = a.id_str;
        log = a.log;
        param = (Properties) a.param.clone();
        set_cookies = new ArrayList<String>();
        set_cookies.addAll(a.set_cookies);
        text = a.text;
        file = a.file;
    }

    void Log(boolean in_window, String s) {
        if (in_window) {
            log.Log(id_str + "  " + s);
        } else {
            System.out.print(id_str + "  " + s);
        }
    }

    public void set_code(int _code) {
        this.code.set_code(_code);
    }

    public void set_version(String v) {
        code.set_version(v);
    }

    public void set_property(String name, String value) {
        param.setProperty(name, value);
    }

    public void set_cookie(String setcookie_line) {
        set_cookies.add(setcookie_line);
    }

    /**
     * Sets the reply contents with file content
     *
     * @param _f
     * @param mime_enc
     */
    public void set_file(File _f, String mime_enc) {
        DateFormat httpformat
                = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
        httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));
        file = _f;

        param.setProperty(NameDictionary.ContentType, mime_enc + "; charset= " + NameDictionary.Encoding);
        param.setProperty(NameDictionary.ContentLength,
                Long.toString(file.length()));
        param.setProperty(NameDictionary.LastModified, httpformat.format(_f.lastModified()));
        param.setProperty(NameDictionary.ETAG, Integer.toString(_f.hashCode()));
    }

    /**
     * Sets the reply text contents of type HTML
     *
     * @param _text
     */
    public void set_text(String _text) {
        DateFormat httpformat
                = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
        httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));

        text = _text;
        param.setProperty(NameDictionary.ContentType, "text/html; charset= " + NameDictionary.Encoding);
        param.setProperty(NameDictionary.ContentLength, Integer.toString(text.getBytes().length));
        param.setProperty(NameDictionary.CacheControl, "no-cache");
        param.setProperty(NameDictionary.LastModified, httpformat.format(System.currentTimeMillis()));
    }

    /**
     * Returns the answer code
     */
    public int get_code() {
        return code.get_code();
    }

    /**
     * Returns a string with the first line of the answer
     */
    public String get_first_line() {
        return code.toString();
    }

    public void buildCookie(String name, String value) {
        set_cookies.add(name + "=" + value + "; path=/");
    }

    /**
     * Returns an iterator over all hearder names
     */
    public Iterator<Object> get_Iterator_parameter_names() {
        return param.keySet().iterator();
    }

    /**
     * Returns the array list with all set_cookies
     */
    public ArrayList<String> get_set_cookies() {
        return set_cookies;
    }

    /**
     * Sets the "Date" header field with the local date in HTTP format
     */
    void set_Date() {
        DateFormat httpformat
                = new SimpleDateFormat("EE, d MMM yyyy HH:mm:ss zz", Locale.UK);
        httpformat.setTimeZone(TimeZone.getTimeZone("GMT"));
        param.setProperty("Date", httpformat.format(new Date()));
    }

    void set_KeepAlive(int time) {
        if (time > 0) {
            param.setProperty(NameDictionary.Connection, NameDictionary.KeepAlive);
            param.setProperty(NameDictionary.KeepAlive, "timeout=" + Integer.toString(time / 1000) + ", max=200");
        } else {
            param.setProperty(NameDictionary.Connection, "close");
        }
    }

    /**
     * Prepares an HTTP answer with an error code
     */
    public void set_error(int _code, String version) {
        set_version(version);
        set_Date();
        code.set_code(_code);
        if (code.get_code_txt() == null) {
            code.set_code(HTTPReplyCode.BADREQ);
        }

        if (!version.equalsIgnoreCase("HTTP/1.0")) {
            param.setProperty("Connection", "close");
        }
        // Prepares a web page with an error description
        String txt = "<HTML>\r\n";
        txt = txt + "<HEAD><TITLE>Error " + code.get_code() + " -- " + code.get_code_txt()
                + "</TITLE></HEAD>\r\n";
        txt = txt + "<H1> Error " + code.get_code() + " : " + code.get_code_txt() + " </H1>\r\n";
        txt = txt + "  by " + param.getProperty("Server") + "\r\n";
        txt = txt + "</HTML>\r\n";

        // Set the header properties
        set_text(txt);
    }

    /**
     * Sends the HTTP reply to the client using 'pout' text device
     *
     * @param pout
     * @param send_data
     * @param echo
     */
    public void send_Answer(PrintStream pout, boolean send_data, boolean echo) throws IOException {
        if (code.get_code_txt() == null) {
            code.set_code(HTTPReplyCode.BADREQ);
        }
        if (echo) {
            Log(true, "Answer: " + code.toString() + "\n");
        }
        pout.print(code.toString() + "\r\n");

        /**
         * Send all headers except set_cookie
         */
        Iterator<Object> aux = get_Iterator_parameter_names();
        String Header, Value;

        while (aux.hasNext()) {
            Header = aux.next().toString();
            Value = param.getProperty(Header);
            pout.print(Header + ": " + Value + "\r\n");
        }

        /**
         * Send set_cookie
         */
        Iterator<String> aux2 = set_cookies.iterator();
        boolean firstline = true;
        while (aux2.hasNext()) {
            if (firstline) {
                pout.print(NameDictionary.setcookie + ": ");
                firstline = false;
            }
            pout.print(aux2.next().toString());
            if (aux2.hasNext()) {
                pout.print("; ");
            } else {
                pout.print("\r\n");
            }
        }

        pout.print("\r\n");

        if (send_data) {

            if (text != null) {
                pout.print(text);
            } else if (file != null) {
                FileInputStream fin = new FileInputStream(file);
                if (fin == null) {
                    Log(true, "Internal error sending answer data\n");
                    return;
                }

                //byte[] data = new byte[fin.available()];
                byte[] data = new byte[524];
                while (fin.read(data) != -1) {
                    pout.write(data);
                }

                fin.close();
            } else if (code.get_code() != HTTPReplyCode.NOTMODIFIED) {
                Log(true, "Internal server error sending answer\n");
            }
        }

        pout.flush();
        if (echo) {
            Log(false, "\n");
        }
    }
}
