
var jwt = require('jsonwebtoken');  //https://npmjs.org/package/node-jsonwebtoken
var secret = 'this is the secret secret secret 12356'; // same secret as in app.js used here to sign the authentication token
var usermodel = require('../models/user.js'); // model for the database data this Object is used to access the database

/*
 * GET home page. First get is responded here with the main page
 */
exports.Signin = function(req, res){
  var templateData = {							//data to set html header (angular module) and page title that is used also for menu
		    angularApp : "rit2app", // to fill the "ng-app" directive in the jade template with the name of the angular module
			pageTitle : "SeeYouHere"
		    }
  res.render('index', templateData);
};
/*
 * POST User sign in. User Sign in POST is treated here
 */	
exports.ValidateUser = function (req,res) {
	  //TODO validate req.body.username and req.body.password (access mongo db and test) 
	  //if is invalid, return 401
	 console.log(req.body.username + " " + req.body.password);
	 
	 usermodel.findOne({username: req.body.username,islogged:{$ne : true}}, function(err,ExistingUser)
				{
					if (err) {
						console.error("ERROR: While inserting user");
						console.error(err);
						res.send(401,'DB error');
					}
					else {
						if(ExistingUser){
							console.log("User already exists");
							if(req.body.password === ExistingUser.password){
								console.log("Login OK!");
								
								usermodel.update({username: ExistingUser.username},{$set:{islogged: 
									true}},function(err, User){ 
									       if (err) { 
									         console.error(err);       
									       }  
									           if (User != null){ //user updated         
									         console.log(User);     
									       } 
									   }); 
								
								var token = jwt.sign(ExistingUser, secret, {expiresInMinutes: 60*5});
								res.json({ token: token });
							}
							else{
								console.log("Bad Login!");
								res.send(200, 'Bad Login');
							}
						}else{
							console.log("NOT Reg!");
							res.send(200, 'Not Register');
						}
					}
				}
	 );
};	
	
/*
 * POST User registration. User registration POST is treated here
 */				
exports.NewUser = function (req,res) { //this should store the posted data in the database
	console.log("received form submission");
	console.log(req.body);
	
	//check if user already exists
	//if user still does not exist
	//create a newUser object instance with the fields defined in the usermodel object TODO
	// save the newUser to the database
	
	usermodel.findOne({username: req.body.username}, function(err,ExistingUser)
	{
		if (err) {
			console.error("ERROR: While inserting user");
			console.error(err);
			res.send(401,'DB error');
		}
		else {
			if(ExistingUser){
				console.log("User already exists");
				console.log(res.data);
				res.send(200,'User Already exists');
			}
			else{
				
				var newUser = new usermodel({
					name : req.body.name,
					email : req.body.email,
					username: req.body.username,
					password: req.body.password,
					islogged: false
					});
				
				
				newUser.save(function(err){
					if (err) {
						console.error("Error on saving new user");
						console.error(err); // log error to Terminal
						res.send(401,'DB error');
					} 
					else {
						console.log("Created a new user!");
						res.send(200,'OK'); //sends in the body of the Http response the string
					}
				});
				
				
			}
		}
	});
	
};

	
