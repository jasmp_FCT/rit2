
/**
 * Express app single page site with chat
 * author: Pedro Amaral 
 */

var express = require('express');

var http = require('http'); //http object to create server
var path = require('path'); //object to deal with paths
var mongoose = require('mongoose'); // to deal with the mongodb database
var expressJwt = require('express-jwt'); //to deal with authentication based in tokens -  HTTP server
var socketioJwt = require('socketio-jwt'); //to deal with authentication based in tokens -  WebSocket	
var secret = 'this is the secret secret secret 12356'; // secret to sign token
var user = require('./models/user'); //schema model to define user data structure in the database
var routes = require('./routes/index.js'); // file with module to deal with the routes for each http request
										 //receives secret for authentication

var app = express(); // Express Application Object

// all environments
app.set('port', process.env.PORT || 3000); // you can change the port to another value here
app.set('views', path.join(__dirname, 'views')); // sets up the path for the Jade Templates 
app.set('view engine', 'jade'); //setup template engine - we're using jade
app.use(express.favicon()); // use default Express tab icon
app.use(express.logger('dev')); // use developer logs

app.use(express.json()); // set up the use of JSON described objects in HTTP messages
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router); // set up Express routing of URL requests
app.use(express.static(path.join(__dirname, 'public')));// setting up the public dir  
app.use('/restricted',expressJwt({secret:secret}));// set up authentication for HTTP requests to "/restricted" url

/*if you wish to restrict the access to some URL 


app.use(function(err, req, res, next){ // if UnauthorizedError occurs send a 401 code back to client
	  if (err.constructor.name === 'UnauthorizedError') {
	    res.send(401, 'Unauthorized');
	  }
	  else console.error(err.stack);
	});
*/
// database connection TODO 
app.db = mongoose.connect('mongodb://127.0.0.1:27017/local'); //local database change if necessary
console.log("connected to database");

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.Signin); //route to deal with the get for the main view (authentication form)
app.post('/authenticate', routes.ValidateUser); //route to deal with the post of the authentication form
app.post('/newuser', routes.NewUser); //route to deal with the post of the register form

var server = http.createServer(app); //HTTP server Object
var io = require('socket.io').listen(server); //WebSocket server Object

var socket = require('./routes/socket.js')(io,socketioJwt,secret); // file with module to deal with Websocket communications 
											// receives the Websocket Object the authentication Object and the secret

server.listen(app.get('port'), function(){   // starts the HTTP server
	  console.log('Express server listening on port ' + app.get('port'));
	});


