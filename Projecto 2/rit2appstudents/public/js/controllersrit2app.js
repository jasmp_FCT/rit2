var controllersrit2app = angular.module('controllersrit2app',['AngularGM']); //controllers module
//We have an individual controller for each different client side view of the app

controllersrit2app.factory('items', function() {
	var myVar = {
			myPos: {}
	};
	return myVar; 

});

//Controller for the register view callback function receives the scope of this controller the http window and location services
controllersrit2app.controller('registerCtrl', function ($scope, $http, $window,$location) { 
	$scope.view = 'Register'; // includes the string name in the scope of this controller
	$scope.isError = false;   // does not show error in page
	$scope.error = '';		// error string starts blank

	//function that deals with the submit button click
	$scope.register = function (){
		//if($scope.user && $scope.password && $scope.username){
		$http						//$scope.user is the object containing form values
		.post('/newuser', $scope.user) //performs an http post with URL /newuser 
		.success(function (data, status, headers, config) { //called if HTTP call was sucessfull  
			console.log(data); //data contains the data part of the HTTP response         
			if(data === 'OK'){
				$location.path("/signin"); 
			}
			else{
				$scope.isError = true;  // show error in page 
				$scope.error = 'Error: User name already exists please try again';
			}

		})
		.error(function (data, status, headers, config) { //if HTTP response was not OK
			// Handle submit errors (show error in page and whatever actions are needed)
			$scope.isError = true;
			$scope.error = 'Error: User name already exists please try again';
			console.log("New User Error!");
		});
		//}
		/*else{
			$scope.isError = true;
			$scope.error = 'Fields empty';			
		}*/
	};

});
/* Controller for the sign in view callback function receives the $scope, $http, $window, $location and $socket services
 */
controllersrit2app.controller('signinCtrl', function ($scope, $http, $window, $location, $socket) {
	$window.sessionStorage.initialized = false; // chat view was never rendered yet

	if ($window.sessionStorage.islogged) {
		$socket.connect(); // connect to the Server WebSocket
		$location.path( "/SeeYouHere" ); //if already logged go to chat view
		console.log("re-directing to chat view because already submitted");
	} 
	else{
		$scope.view = 'Sign In';  //initial view state
		$scope.isError = false;	  
	}

	/*
	 * Function that deals with a click in the submit button
	 */
	$scope.submit = function () {
		//call HTTP post with form contents   
		//TODO deal with response 	  
		$http						//$scope.user is the object containing form values
		.post('/authenticate', $scope.user) //performs an http post with URL /authenticate
		.success(function (data, status, headers, config) { //called if HTTP call was sucessfull  
			console.log(data); //data contains the data part of the HTTP response
			if(data.token){	  //If success:
				//$window.sessionStorage.islogged = true; to indicate user already logged
				$window.sessionStorage.islogged = true;
				$window.sessionStorage.username = $scope.user.username;
				$window.sessionStorage.initialized = false;
				//store token if success so that the authInterceptor service and the socket service can use it for authentication
				$window.sessionStorage.token = data.token;

				$socket.connect();
				//Connect socket and re-route to chat view
				$location.path("/SeeYouHere");
			}
			else{
				$scope.isError = true;
				$scope.error = 'Error: Bad Login';
			}
		})
		.error(function (data, status, headers, config) { //if HTTP response was not OK
			// Erase the token if the user fails to login and Handle errors here
			delete $window.sessionStorage.token;
			$scope.isError = true;
			$scope.error = 'Error: Please Contact Admin';
			console.log("Signin Error!");
		});  
	};
});

/* Controller for the chat view callback function receives the $scope, $http, $window, $location and $socket services
 */

var myPosition;

controllersrit2app.controller('SeeYouHereCtrl', function ($scope, $http, $window, $socket, $location, items) {
	$scope.messages = [];	//messages array initialization
	$scope.showchat = false; // initially view only shows online users 
	$scope.UserName = $window.sessionStorage.username; // show username in view
	///after getting the partial view from the server and upon controller start
	var userJSON = [];
	var msgUsers = [];

	var giveMSG = function(msgArr, from, to){
		var newArr=[],i;
		for(i =0;i< msgArr.length; i++){
			if((msgArr[i].from === from && msgArr[i].to === to)){
				newArr.push({user: msgArr[i].from, text: msgArr[i].message});
			}
			if((msgArr[i].from === to && msgArr[i].to === from)){
				newArr.push({user: msgArr[i].from, text: msgArr[i].message});
			}
		}
		return newArr;
	};


	var buildVector = function(arr){
		var i,l = arr.length;
		var newArr = [];
		for(i =0; i< l; i++){
			newArr.push(arr[i].username);
		}

		return newArr;
	};

	var remove = function(origArr,username){
		var origLen = origArr.length,
		x, found= false;

		for(x = 0 ; x < origLen; x++){
			if(origArr[x] === username){
				found=true;
				break;
			}			
		}
		if(found){
			origArr.splice(x,1);
		}

		return origArr;
	};


	if ($window.sessionStorage.initialized != true){ // only emit the event one time
		$socket.emit('newUser:username', { username: $window.sessionStorage.username }); //send new user event via WebSocket to server 
		$window.sessionStorage.initialized = true;
	}

	// Socket event listeners - define action when receiving several different types of messages via the socket

	$socket.on('init', function (data) { //initial event receives the list of other online users
		console.log(JSON.stringify(data));
		userJSON = buildVector(data);
		$scope.users = userJSON;
	});


	$socket.on('send:message', function (message) { // receiving a message from the server add message to messages scope
		// show message in view
		console.log('From : '+ message.from +' ' + message.message);
		msgUsers.push(message);

		if(!$scope.showchat){
			$scope.showchat = true;
		}

		if(message.from !== $scope.talkingto){
			$scope.talkingto = message.from;
		}
		message.coord.isMe = false;
		var index = $scope.myMarkers.map(function(e) { return e.id; }).indexOf(message.from);
		if(index !== -1){
			console.log("update marker");
			$scope.myMarkers[index].pos.lat =message.coord.pos.lat;
			$scope.myMarkers[index].pos.lng = message.coord.pos.lng;
		}
		else{
			console.log("new marker");
			$scope.myMarkers.push(message.coord);
		}

		$scope.messages = giveMSG(msgUsers,message.from ,message.to);
		$scope.$broadcast('gmMarkersUpdate', 'myMarkers');
	});

	//add methods to handle other received events suggestions:

	// event received when a user logs in the server

	$socket.on('user:join', function (data) { 
		console.log(JSON.stringify(data));
		userJSON.push(data.username);
		$scope.users = userJSON;
	});

	// Method that deals with a click in the send message button 
	$scope.sendMessage = function () { // sends a message via websocket to the server 
		$socket.emit('send:message', {to : $scope.talkingto ,from : $window.sessionStorage.username , message : $scope.message, coord: items.myPos });
		// add the message to our model locally to appear in the view
		// clear message box
		msgUsers.push({to : $scope.talkingto ,from : $window.sessionStorage.username , message : $scope.message});
		$scope.message = '';
		$scope.messages = giveMSG(msgUsers,$window.sessionStorage.username ,$scope.talkingto);
	};

	//Method that deals with a click in the close connection button   
	$scope.closeConnection = function(){  
		$socket.emit('user:coord', { coord:  items.myPos , to: $scope.talkingto});
		$scope.messages = [];
		$scope.showchat=false;
	}; 

	//Method that deals with a click in the logout button
	$scope.logout = function () { 
		//clears session variables
		console.log("logout user : " + $window.sessionStorage.username);
		$socket.emit('user:logout', { username: $window.sessionStorage.username });
		delete $window.sessionStorage.username;
		delete $window.sessionStorage.islogged;
		delete $window.sessionStorage.token;
		delete $window.sessionStorage.initialized;
		// disconnects WebSocket
		$socket.disconnect();
		// re-route to sign in view		
		$location.path("/signin");
	};	 

	// event received when a user logs out the server
	$socket.on('user:out', function (data) { //initial event receives the list of other online users	    
		console.log(JSON.stringify(userJSON));
		userJSON = remove(userJSON, data.username);
		$scope.users = userJSON;
	});

	$socket.on('user:coordCheck', function (data) {
		console.log(JSON.stringify(data));
		var index = $scope.myMarkers.map(function(e) { return e.id; }).indexOf(data.coord.id);
		if(index !== -1){
			$scope.myMarkers.splice(index,1);
			$scope.$broadcast('gmMarkersUpdate', 'myMarkers');
		}
	});
	
	$socket.on('user:disconected', function (data) {
		console.log(JSON.stringify(data));
		var index = $scope.myMarkers.map(function(e) { return e.id; }).indexOf(data.username);
		userJSON = remove(userJSON, data.username);
		if(index !== -1){
			$scope.myMarkers.splice(index,1);
			$scope.$broadcast('gmMarkersUpdate', 'myMarkers');
		}
	});

	//Method that deals with a click in one of the online usernames, clicked username is input parameter
	$scope.talkto = function(username){
		console.log("clicked in user: " + username);
		if(username !== $scope.talkingto ){
			$scope.talkingto = username;
		}
		$scope.showchat=true;
		$scope.messages = giveMSG(msgUsers,$window.sessionStorage.username ,$scope.talkingto);
	};


	/*************************************************************** GOOGLE CONTROLLER ********************************************************/
	$scope.myMarkers = [];
	$scope.options = {
			map: {
				center: new google.maps.LatLng(0, 0),
				zoom: 7
			},
			isMe: {
				icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/green-dot.png',
			},
			notMe: {
				icon: 'https://maps.gstatic.com/mapfiles/ms2/micons/red-dot.png',
			}
	};

	var opt = {
			enableHighAccuracy: true,
			timeout: 5000,
			maximumAge: 0
	};

	$scope.$on('gmMapIdle', function(event, mapId) {
		if (mapId === 'simpleMap') {

			function errorGEO(err){
				console.log(err.message);
				$scope.center = new google.maps.LatLng(0, 0);
				items.myPos= {
						id: $scope.UserName,
						name:  $scope.UserName,
						pos: {
							lat: 0, 
							lng: 0
						},
						isMe: true
				};

				$scope.myMarkers.push(items.myPos);
				$scope.$apply();
			};

			function successGEO (pos){
				$scope.center = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
				items.myPos= {
						id: $scope.UserName,
						name:  $scope.UserName,
						pos: {
							lat: pos.coords.latitude, 
							lng: pos.coords.longitude
						},
						isMe: true
				};

				$scope.myMarkers.push(items.myPos);
				$scope.$apply();
			};

			navigator.geolocation.getCurrentPosition(successGEO, errorGEO,opt);

		}
	});

	$scope.getMarkerOptions = function(person) {
		var opts = {title: person.name};
		if (person.isMe) {
			return angular.extend(opts, $scope.options.isMe );
		} else {
			return angular.extend(opts,  $scope.options.notMe );
		}
	};

});